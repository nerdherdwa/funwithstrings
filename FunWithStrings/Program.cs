﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            StringsAreImmutable();
            ComparinStrings();
            StringsHaveLengthAndIndexes();
            CasingAndWhiteSpace();
            UsefulCharacterMethods();
            EditingStrings();
            SplitAndJoin();
        }

        private static void SplitAndJoin()
        {
            Console.WriteLine("Split and Join Strings");
            Console.WriteLine();

            string s1 = "This is a very long sentence that needs to be split";

            string[] words = s1.Split(' ');

            foreach (var word in words)
            {
                Console.WriteLine(word);
            }

            string s2 = string.Join(", ", words);
            Console.WriteLine("The new structure is: {0}", s2);

            Console.ReadLine();
            Console.Clear();
        }

        private static void EditingStrings()
        {
            Console.WriteLine("Editing Strings");
            Console.WriteLine();

            string s1 = "     Got some extra spaces?    ";
            Console.WriteLine("With extra spaces: {0}", s1);
            s1 = s1.Trim();
            Console.WriteLine("Trimmed the spaces: {0}",s1);

            s1 = s1.Replace("Got", "Have");
            Console.WriteLine("Replaced got with have in string: {0}",s1);

            Console.ReadLine();
            Console.Clear();
        }

        private static void UsefulCharacterMethods()
        {
            Console.WriteLine("Useful Character Methods");
            Console.WriteLine();

            Console.WriteLine("Enter a character?");
            string storeValue = Console.ReadLine();
            if (storeValue.Length == 1) { 
            char c1 = storeValue[0];


            Console.WriteLine("The character: {0}", c1);
            Console.WriteLine("IsDigit: {0}", char.IsDigit(c1));
            Console.WriteLine("IsLetter: {0}", char.IsLetter(c1));
            Console.WriteLine("IsLetterOrDigit: {0}", char.IsLetterOrDigit(c1));
            Console.WriteLine("IsLower: {0}", char.IsLower(c1));
            Console.WriteLine("IsUpper: {0}", char.IsUpper(c1));
            Console.WriteLine("IsPunctuation: {0}", char.IsPunctuation(c1));
            Console.WriteLine("IsWhiteSpace: {0}", char.IsWhiteSpace(c1));

            Console.ReadLine();
            Console.Clear();
            }

        }

        private static void CasingAndWhiteSpace()
        {
            Console.WriteLine("Casing and White Space");
            Console.WriteLine();

            string s1 = "";
            Console.WriteLine("Does the string have data? {0}", string.IsNullOrEmpty(s1));

            s1 = "AbCdEfG";
            Console.WriteLine("{0} ToUpper: {1} ToLower: {2}", s1, s1.ToUpper(), s1.ToLower());

            Console.ReadLine();
            Console.Clear(); 
        }

        private static void StringsHaveLengthAndIndexes()
        {
            Console.WriteLine("Strings Have Length and Indexes");
            Console.WriteLine();

            string name = "Michael";

            // access single characters like arrays
            Console.WriteLine("The First letter of your name is: {0}", name[0]);

            // print the name in reverse using for loop

            for (int i = name.Length-1; i >= 0; i--)
            {
                Console.WriteLine("{0}",name[i]);
            }

            Console.WriteLine();

            // Substring takes a string apart
            // one number gets a string after the index
            Console.WriteLine("Everything at or after position 1: {0}", name.Substring(1));

            // two numbers starts at a position and reads the next N characters
            Console.WriteLine("The last two characters are: {0}", name.Substring(name.Length - 2, 2));

            Console.ReadLine();
            Console.Clear();
        }

        private static void ComparinStrings()
        {
            Console.WriteLine("Comparing Strings");
            Console.WriteLine();
            string s1 = "hi";
            string s2 = "HI";

            // using == or != is case sensitive
            if (s1 != s2)
            {
                Console.WriteLine("s1 is not equal to s2");
            }

            // the .Equals() method can take an option to ignore casing
            if (s1.Equals(s2,StringComparison.CurrentCultureIgnoreCase))
            {
                Console.WriteLine("s1 is equal to s2");
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void StringsAreImmutable()
        {
            Console.WriteLine("Strings are Immutable");
            Console.WriteLine();
            // immutable is a fancy word that means that whenever you change a string a new string is returned
            string s1 = "This is my name!";

            // call ToUpper and then print the value of s1
            s1.ToUpper();
            Console.WriteLine("s1: {0}", s1);

            // assign the result of ToUpper to the existing variable
            s1 = s1.ToUpper();
            Console.WriteLine("s1: {0}", s1);

            Console.ReadLine();
            Console.Clear();
        }
    }
}
